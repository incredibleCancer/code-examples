<?php

/**
 * @property JourneyProgram $journeyProgram
 * @property Country $country
 * @property Partner $partner
 */
class JourneyProgramSupport extends ActiveRecord
{
    /** @var int */
    public $id;

    /** @var int */
    public $journeyProgramId;

    /** @var int */
    public $countryId;

    /** @var int */
    public $partnerId;

    /**
     * @param string $className
     * @return ActiveRecord|JourneyProgramDay
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'journey_program_support';
    }

    public function primaryKey() {
        return 'id';
    }

    public function relations()
    {
        $alias = $this->getTableAlias();

        return [
            'journeyProgram' => [self::HAS_ONE, JourneyProgram::class, ['id' => 'journeyProgramId'], 'alias' => "{$alias}_journeyProgram"],
            'country' => [self::HAS_ONE, Country::class, ['id' => 'countryId'], 'alias' => "{$alias}_country"],
            'partner' => [self::HAS_ONE, Partner::class, ['id' => 'partnerId'], 'alias' => "{$alias}_partner"],
        ];
    }

    public function rules() {
        return [
            ['journeyProgramId', 'exist', 'className' => JourneyProgram::class, 'attributeName' => 'id'],
            ['journeyProgramId', 'default', 'value' => null],

            ['countryId', 'exist', 'className' => Country::class, 'attributeName' => 'id'],
            ['countryId', 'default', 'value' => null],

            ['partnerId', 'exist', 'className' => Partner::class, 'attributeName' => 'id'],
            ['partnerId', 'default', 'value' => null],
        ];
    }

    public function attributeLabels() {
        return array(
            '' => ''
        );
    }
}
