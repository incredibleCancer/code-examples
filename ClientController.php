<?php

class ClientController extends BaseController
{
    /** @var NotifyService */
    protected $notifyService;

    /** @var SubscriberService */
    protected $subscriberService;

    /** @var ClientTokenService */
    protected $clientTokenService;

    /** @var string */
    protected $siteUrl;

    public function init()
    {
        parent::init();

        $this->notifyService = Yii::app()->getComponent('notifyService');
        $this->subscriberService = Yii::app()->getComponent('subscriberService');
        $this->clientTokenService = Yii::app()->getComponent('clientTokenService');
        $this->siteUrl = Yii::app()->params->itemAt('siteUrl');
    }

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny', 'users' => ['*']],
        ];
    }

    /**
     * @param int $clientId
     * @param array $with
     * @return Client
     * @throws CHttpException
     */
    protected function loadClientModel($clientId, array $with = [])
    {
        $model = Client::model();
        if (($client = $model->with($with)->findByPk($clientId)) === null) {
            throw new CHttpException(404, "Client with ID {$clientId} doesn't found");
        }

        return $client;
    }

    public function actionWorkHistory($clientId)
    {
        $client = $this->loadClientModel($clientId);

        $this->breadcrumbs = [
            'Клиенты' => ['index'],
            $client->fullName => ['client/edit', 'clientId' => $client->id],
            'История работы с клиентом'
        ];

        $this->render('work-history', [
            'client' => $client
        ]);
    }

    public function actionWorkHistoryCreate($clientId)
    {
        $client = $this->loadClientModel($clientId);

        $this->breadcrumbs = [
            'Клиенты'                   => ['index'],
            $client->fullName           => ['client/edit', 'clientId' => $client->id],
            'История работы с клиентом' => ['workHistory', 'clientId' => $client->id],
            'Создание заметки в истории',
        ];

        $model = new ClientWorkHistory();

        if (isset($_POST[get_class($model)])) {
            $model->attributes = $_POST[get_class($model)];
            $model->clientId = $clientId;

            if ($model->save()) {
                Yii::app()->user->setFlash('workHistoryEdit', 'success');
                $this->redirect($this->createUrl('workHistoryEdit', [
                    'clientId' => $client->id,
                    'workHistoryId' => $model->id
                ]));
            }
        }

        $this->render('work-history-item', [
            'client' => $client,
            'model' => $model,
            'requests' => $client->requests,
            'workTypes' => SettingClientWorkType::model()->findAll()
        ]);
    }

    public function actionWorkHistoryEdit($clientId, $workHistoryId)
    {
        $client = $this->loadClientModel($clientId);

        if (null === $model = ClientWorkHistory::model()->findByPk($workHistoryId)) {
            throw new CHttpException(404);
        }

        $this->breadcrumbs = [
            'Клиенты'                    => ['index'],
            $client->fullName            => ['client/edit', 'clientId' => $client->id],
            'История работы с клиентом'  => ['workHistory', 'clientId' => $client->id],
            "Заметка: {$model->title}" => ['workHistoryEdit', 'clientId' => $clientId, 'workHistoryId' => $model->id],
            'Редактирование',
        ];

        if (isset($_POST[get_class($model)])) {
            $model->attributes = $_POST[get_class($model)];

            if ($model->save()) {
                Yii::app()->user->setFlash('workHistoryEdit', 'success');

                $this->redirect($this->createUrl('workHistoryEdit', [
                    'clientId' => $client->id,
                    'workHistoryId' => $model->id
                ]));
            }
        }

        $this->render('work-history-item', [
            'client' => $client,
            'model' => $model,
            'requests' => $client->requests,
            'workTypes' => SettingClientWorkType::model()->findAll()
        ]);
    }

    public function actionWorkHistoryRemove($clientId, $workHistoryId)
    {
        $this->loadClientModel($clientId);

        if (null === $model = ClientWorkHistory::model()->findByPk($workHistoryId)) {
            throw new CHttpException(404);
        }

        $model->delete();

        return $this->redirect($this->createUrl('workHistory', [
            'clientId' => $clientId
        ]));
    }
}
