<?php

/**
 * Небольшая вырезка из роутинга, чтобы можно было представить, как их организовывал.
 * Для api стремление быть ближе к REST, но не использовали HTTP-методы для большего соответствия REST'у,
 * в Symfony экшены api пишу по HTTP-методам
 */

return [
    '/dictionaries/hotels/<hotelId:\d+>/statistics'                 => 'dictionary/hotelStatistics',

    '/dictionaries/hotels/<hotelId:\d+>/rooms/'                     => 'dictionary/hotelRooms',
    '/dictionaries/hotels/<hotelId:\d+>/rooms/new/'                 => 'dictionary/hotelRoomsNew',
    '/dictionaries/hotels/<hotelId:\d+>/rooms/<itemId:\d+>/edit/'   => 'dictionary/hotelRoomsEdit',
    '/dictionaries/hotels/<hotelId:\d+>/rooms/<itemId:\d+>/delete/' => 'dictionary/hotelRoomsDelete',
    '/dictionaries/<_a>/new/'                                       => 'dictionary/<_a>New',
    '/dictionaries/<_a>/<itemId:\d+>/edit/'                         => 'dictionary/<_a>Edit',
    '/dictionaries/<_a>/<itemId:\d+>/delete/'                       => 'dictionary/<_a>Delete',
    '/dictionaries/<_a>/<itemId:\d+>.pdf'                           => 'dictionary/<_a>Pdf',
    '/dictionaries/<_a>/'                                           => 'dictionary/<_a>',

    '/api/hotels/'                                              => 'api/hotels',
    '/api/hotels/<hotelId:\d+>/'                                => 'api/hotel',
    '/api/hotels/<hotelId:\d+>/rooms/'                          => 'api/hotelRooms',
    '/api/hotels/<hotelId:\d+>/rooms/<roomId:\d+>/prices/'      => 'apiPrice/getRoomPrices',

    '/requests/<requestId:\d+>/payment-limits/' => 'request/paymentLimits',
    '/requests/<requestId:\d+>/payment-limits/create' => 'request/paymentLimitsCreate',
    '/requests/<requestId:\d+>/payment-limits/<paymentLimitId:\d+>/edit' => 'request/paymentLimitsEdit',
    '/requests/<requestId:\d+>/payment-limits/<paymentLimitId:\d+>/delete' => 'request/paymentLimitsDelete',

    '/requests/<requestId:\d+>/clients/' => 'request/clients',
    '/requests/<requestId:\d+>/clients/create' => 'request/clientsCreate',
    '/requests/<requestId:\d+>/clients/<clientId:\d+>/edit' => 'request/clientsEdit',
    '/requests/<requestId:\d+>/clients/<clientId:\d+>/remove' => 'request/clientsRemove',

    '/requests/<requestId:\d+>/journey-programs/' => 'request/journeyPrograms',
    '/requests/<requestId:\d+>/journey-programs/create' => 'request/journeyProgramCreate',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/edit' => 'request/journeyProgramEdit',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/remove' => 'request/journeyProgramRemove',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/visibility' => 'request/journeyProgramVisibility',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>.pdf' => ['request/journeyProgramPdf', 'urlSuffix' => ''],
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/days/create' => 'request/journeyProgramDayCreate',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/days/<dayId:\d+>/edit' => 'request/journeyProgramDayEdit',
    '/requests/<requestId:\d+>/journey-programs/<programId:\d+>/days/<dayId:\d+>/remove' => 'request/journeyProgramDayRemove',

    '/clients/<clientId:\d+>/work-history/' => 'client/workHistory',
    '/clients/<clientId:\d+>/work-history/create' => 'client/workHistoryCreate',
    '/clients/<clientId:\d+>/work-history/<workHistoryId:\d+>' => 'client/workHistoryEdit',
    '/clients/<clientId:\d+>/work-history/<workHistoryId:\d+>/remove' => 'client/workHistoryRemove',
];
