<?php

/**
 * Небольшая вырезка из контроллера справочника с экшеном, отдающим статистику по заказам номеров в отеле за период.
 * Рядом приложил класс сервиса HotelStatisticsService
 */

class DictionaryController extends BaseController
{
    /** @var HotelRoomPricesService */
    protected $hotelRoomPricesService;

    /** @var HotelStatisticsService */
    protected $hotelStatisticsService;

    /** @var DateFormatterEx */
    protected $dateFormatterEx;

    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['deny', 'users' => ['*']],
        ];
    }

    public function init()
    {
        parent::init();
        $this->hotelRoomPricesService = Yii::app()->hotelRoomPricesService;
        $this->hotelStatisticsService = Yii::app()->hotelStatisticsService;
        $this->dateFormatterEx = Yii::app()->getComponent('dateFormatterEx');
    }

    public function actionHotelStatistics($hotelId)
    {
        $hotel = $this->findHotel($hotelId);
        $request = Yii::app()->request;
        $city = $hotel->city;
        $country = $city->country;
        $filters = [];
        $selectedStatuses = [];

        $this->breadcrumbs = [
            'Справочники'               => ['index'],
            'Регионы'                   => ['regions'],
            $country->region->title     => ['countries', 'regionId' => $country->regionId],
            "{$country->title}: Города" => ['cities', 'countryId' => $country->id],
            "{$city->title}: Отели"     => ['hotels', 'cityId' => $city->id],
            $hotel->title               => ['hotel', 'hotelId' => $hotel->id],
            'Статистика',
        ];

        if ($dateFrom = $request->getParam('date_from')) {
            $filters['date_from'] = $this->dateFormatterEx->formatDateToMysqlFormat($dateFrom);
        }

        if ($dateTo = $request->getParam('date_to')) {
            $filters['date_to'] = $this->dateFormatterEx->formatDateToMysqlFormat($dateTo);
        }

        if ($requestStatuses = $request->getParam('request_status')) {
            $filters['request_statuses'] = $requestStatuses;
            $selectedStatuses = array_flip($filters['request_statuses']);
        }

        $requestedRooms = $this->hotelStatisticsService->getRequestedRoomsByFilters($hotel, $filters);

        foreach ($requestedRooms as $room) {
            $room->requestDestinationsCount = $this->hotelStatisticsService->countRequestedRoomsByFilters($room, $filters);
            $room->requestedNightsCount = $this->hotelStatisticsService->countNightsInRequestedRoomsByFilters($room, $filters);
        }

        $this->render('hotel-statistics', [
            'hotel' => $hotel,
            'requested_rooms' => $requestedRooms,
            'filters' => $filters,
            'request_statuses' => RequestStatus::model()->findAll(),
            'selected_statuses' => $selectedStatuses
        ]);
    }

    /**
     * @param int $itemId
     * @param array $with
     * @throws CDbException
     * @throws CHttpException
     * @return Hotel
     */
    protected function findHotel($itemId, $with = [])
    {
        $model = Hotel::model();
        if (!empty($with)) {
            $model->with($with);
        }
        if (($hotel = $model->findByPk($itemId)) === null) {
            throw new CHttpException(404);
        }

        return $hotel;
    }

}
