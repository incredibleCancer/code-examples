<?php

class AcquiringController extends BaseController
{
    /** @var CashApiComponent */
    protected $cashApi;

    /** @var ClientTokenService */
    protected $clientTokenService;

    public function init()
    {
        parent::init();

        $this->cashApi = Yii::app()->getComponent('cashApi');
        $this->clientTokenService = Yii::app()->getComponent('clientTokenService');
    }

    public function actionOrderCreate($clientId, $paymentId)
    {
        $this->loadClientModel($clientId);

        /** @var CHttpRequest $request */
        $request = Yii::app()->request;

        $token = $request->getQuery('token');
        /** @var ClientTransaction $transaction */
        $transaction = ClientTransaction::model()->findByPk($paymentId);

        /** @var ClientTokenService $clientTokenService */
        $clientTokenService = Yii::app()->getComponent('clientTokenService');

        if (!$clientTokenService->checkToken($transaction->client, $token)) {
            throw new CHttpException('403');
        }

        $order = new ClientTransactionOrder();

        $order->clientId = $transaction->clientId;
        $order->transactionId = $transaction->id;
        $order->ekSuccessUrl = $request->getQuery('success_url');
        $order->ekFailUrl = $request->getQuery('fail_url');


        if (!$order->save()) {
            $this->redirect($order->ekFailUrl);
        }

        $order = ClientTransactionOrder::model()->findByPk($order->id);


        $order->tssSuccessUrl = $this->createAbsoluteUrl('acquiring/orderSuccess', [
            'clientId' => $clientId,
            'paymentId' => $paymentId,
            'orderId' => $order->id
        ]);

        $order->tssFailUrl = $this->createAbsoluteUrl('acquiring/orderFail', [
            'clientId' => $clientId,
            'paymentId' => $paymentId,
            'orderId' => $order->id
        ]);

        $response = $this->cashApi->createOrder($order);

        if (null === $response) {
            $this->redirect($order->ekFailUrl);
        }


        $order->billingId = $response['id'];
        $order->paymentUrl = $response['payment_url'];
        $order->status = $response['status'];
        $order->expirationDate = $response['expired_at'];
//        $order->orderData = json_encode($request->getParam('payment_info'));
        $order->save();

        $transaction->orderId = $order->id;
        $transaction->save();

        $this->redirect($order->paymentUrl);
    }

    public function actionOrderSuccess($clientId, $paymentId, $orderId)
    {
        $this->loadClientModel($clientId);

        /** @var ClientTransaction $transaction */
        $transaction = ClientTransaction::model()->findByPk($paymentId);

        if (null === $transaction) {
            throw new CHttpException(404, "Not found");
        }

        /** @var ClientTransactionOrder $order */
        $order = ClientTransactionOrder::model()->findByPk($orderId);

        if (null === $order) {
            throw new CHttpException(404, "Not found");
        }

        $response = $this->cashApi->getOrderStatus($order);

        if (null === $response) {
            $this->redirect($order->ekFailUrl);
        }

        $order->status = $response['status'];

        if ($response['status'] === ClientTransactionOrder::STATUS_SUCCESS) {
            $transaction->orderId = $order->id;
            $transaction->setPaymentConfirmationIssuedAt(new DateTime());
            $transaction->paid = true;
            $transaction->save();
        }

        $order->modifiedAt = date('Y-m-d H:i:s');
        $order->save();
//        $order->orderData = json_encode($request->getParam('payment_info'));

        $this->redirect($order->ekSuccessUrl);
    }

    public function actionOrderFail($clientId, $paymentId, $orderId)
    {
        $this->loadClientModel($clientId);

        /** @var ClientTransaction $transaction */
        $transaction = ClientTransaction::model()->findByPk($paymentId);

        if (null === $transaction) {
            throw new CHttpException(404, "Not found");
        }

        /** @var ClientTransactionOrder $order */
        $order = ClientTransactionOrder::model()->findByPk($orderId);

        if (null === $order) {
            throw new CHttpException(404, "Not found");
        }

        $request = Yii::app()->request;

        $order->status = $request->getParam('status');
        $order->modifiedAt = date('Y-m-d H:i:s');
        $order->orderData = json_encode($request->getParam('payment_info'));

        $this->redirect($order->ekFailUrl);
    }

    /**
     * @param int $clientId
     * @param array $with
     * @return Client
     * @throws CHttpException
     */
    protected function loadClientModel($clientId, array $with = [])
    {
        $model = Client::model();
        if (($client = $model->with($with)->findByPk($clientId)) === null) {
            throw new CHttpException(404, "Client with ID {$clientId} doesn't found");
        }

        return $client;
    }
}

