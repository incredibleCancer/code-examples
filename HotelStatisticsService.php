<?php

class HotelStatisticsService extends CComponent
{
    public function init()
    {
    }

    /**
     * @param Hotel $hotel
     * @param array $filters
     * @return HotelRoom[]
     */
    public function getRequestedRoomsByFilters(Hotel $hotel, array $filters)
    {
        $requestedRooms = HotelRoom::model();
        $alias = $requestedRooms->getTableAlias();

        $criteria = new CDbCriteria();

        $criteria->with = [
            'requestDestinations',
            'requestDestinationsCount',
            'requestDestinations.request' => [
                'alias' => 'request'
            ]
        ];

        if (array_key_exists('date_from', $filters) && null !== $filters['date_from']) {
            $criteria->addCondition("{$alias}_destinations.startedAt > :date_from");
            $criteria->params['date_from'] = $filters['date_from'];
        }

        if (array_key_exists('date_to', $filters) && null !== $filters['date_to']) {
            $criteria->addCondition("{$alias}_destinations.finishedAt < :date_to");
            $criteria->params['date_to'] = $filters['date_to'];
        }

        if (array_key_exists('request_statuses', $filters) && !empty($filters['request_statuses'])) {
            $criteria->addInCondition('request.statusId', $filters['request_statuses']);
        }

        $criteria->group = "{$alias}.title";

        $requestedRooms->setDbCriteria($criteria);

        return  $requestedRooms->findAllByAttributes([
            'hotelId' => $hotel->id
        ]);
    }

    /**
     * @param HotelRoom $room
     * @param array $filters
     * @return int
     */
    public function countRequestedRoomsByFilters(HotelRoom $room, array $filters)
    {
        $countRoom = RequestDestination::model();

        $criteria = new CDbCriteria();

        $criteria->with = [
            'hotelRoom',
            'request' => [
                'alias' => 'request'
            ]
        ];

        $alias = $countRoom->getTableAlias();

        if (array_key_exists('date_from', $filters) && null !== $filters['date_from']) {
            $criteria->addCondition("{$alias}.startedAt > :date_from");
            $criteria->params['date_from'] = $filters['date_from'];
        }

        if (array_key_exists('date_to', $filters) && null !== $filters['date_to']) {
            $criteria->addCondition("{$alias}.finishedAt < :date_to");
            $criteria->params['date_to'] = $filters['date_to'];
        }

        if (array_key_exists('request_statuses', $filters) && !empty($filters['request_statuses'])) {
            $criteria->addInCondition('request.statusId', $filters['request_statuses']);
        }

        $countRoom->setDbCriteria($criteria);

        return (int) $countRoom->countByAttributes([
            'hotelRoomId' => $room->id
        ]);
    }

    /**
     * @param HotelRoom $room
     * @param array $filters
     * @return int
     */
    public function countNightsInRequestedRoomsByFilters(HotelRoom $room, array $filters)
    {
        $destinations = RequestDestination::model();
        $criteria = new CDbCriteria();

        $alias = $destinations->getTableAlias();

        $criteria->with = [
            'hotelRoom',
            'request' => [
                'alias' => 'request'
            ]
        ];

        if (array_key_exists('date_from', $filters) && null !== $filters['date_from']) {
            $criteria->addCondition("{$alias}.startedAt > :date_from");
            $criteria->params['date_from'] = $filters['date_from'];
        }

        if (array_key_exists('date_to', $filters) && null !== $filters['date_to']) {
            $criteria->addCondition("{$alias}.finishedAt < :date_to");
            $criteria->params['date_to'] = $filters['date_to'];
        }

        if (array_key_exists('request_statuses', $filters) && !empty($filters['request_statuses'])) {
            $criteria->addInCondition('request.statusId', $filters['request_statuses']);
        }

        $destinations->setDbCriteria($criteria);

        $destinations = $destinations->findAllByAttributes([
            'hotelRoomId' => $room->id
        ]);

        $nights = 0;

        foreach ($destinations as $destination) {
            $finishedDate = new DateTime($destination->finishedAt);
            $startDate = new DateTime($destination->startedAt);

            $nights += $finishedDate->diff($startDate)->days;
        }

        return $nights;
    }
}
