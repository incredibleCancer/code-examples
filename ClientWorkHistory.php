<?php

/**
 * @property Client $client
 * @property Request $request
 * @property SettingClientWorkType $workType
 */
class ClientWorkHistory extends ActiveRecord
{
    const FIELD_CLIENT = 'ClientWorkHistory.client';
    const FIELD_REQUEST = 'ClientWorkHistory.request';
    const FIELD_WORK_TYPE = 'ClientWorkHistory.workType';

    /** @var int */
    public $id;

    /** @var int */
    public $clientId;

    /** @var int */
    public $requestId;

    /** @var int */
    public $workTypeId;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string */
    public $comment;

    /** @var string */
    public $date;

    /** @var string */
    public $createdAt;

    /** @var string */
    public $modifiedAt;

    /**
     * @param string $className
     * @return ClientWorkHistory|ActiveRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'client_work_history';
    }

    /**
     * @return string
     */
    public function primaryKey()
    {
        return 'id';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['title', 'required'],

            ['description', 'default'],

            ['comment', 'default'],

            ['clientId', 'exists', 'className' => Client::class, 'attributeName' => 'id', 'allowEmpty' => false],

            ['requestId', 'default'],
            ['requestId', 'exists', 'className' => Request::class, 'attributeName' => 'id', 'allowEmpty' => true],

            ['workTypeId', 'required'],
            ['workTypeId', 'exists', 'className' => SettingClientWorkType::class, 'attributeName' => 'id', 'allowEmpty' => false],

            ['date', 'date', 'format' => 'yyyy-MM-dd'],
            ['date', 'default', 'value' => null],

            ['createdAt', 'date', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            ['createdAt', 'default', 'value' => null],

            ['modifiedAt', 'date', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            ['modifiedAt', 'default', 'value' => null],
        ];
    }


    /**
     * @return array
     */
    public function relations()
    {
        return [
            'client' => [self::HAS_ONE, Client::class, ['id' => 'clientId']],
            'workType' => [self::HAS_ONE, SettingClientWorkType::class, ['id' => 'workTypeId']],
            'request' => [self::HAS_ONE, Request::class, ['id' => 'requestId']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'description' => 'Описание работ',
            'clientId' => 'Клиент',
            'requestId' => 'Заявка',
            'workTypeId' => 'Вид работы с клиентом',
            'date' => 'Когда проведена работа',
            'comment' => 'Результат'
        ];
    }

    /**
     * @param string $date
     */
    public function setDate($date) {
        $this->date = Yii::app()->dateFormatterEx->formatDateToMysqlFormat($date);
    }

    /**
     * @return string
     */
    public function getDate() {
        return $this->date;
    }
}
