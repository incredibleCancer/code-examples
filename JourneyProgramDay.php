<?php

/**
 * @property JourneyProgram $journeyProgram
 */
class JourneyProgramDay extends ActiveRecord
{
    /** @var int */
    public $id;

    /** @var int */
    public $journeyProgramId;

    /** @var string */
    public $date;

    /** @var string */
    public $plan;

    /** @var bool */
    public $additional;

    /**
     * @param string $className
     * @return ActiveRecord|JourneyProgramDay
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'journey_program_day';
    }

    public function primaryKey() {
        return 'id';
    }

    public function relations()
    {
        return [
            'journeyProgram' => [self::HAS_ONE, JourneyProgram::class, ['id' => 'journeyProgramId']],
        ];
    }

    public function rules() {
        return [
            ['journeyProgramId', 'exist', 'className' => JourneyProgram::class, 'attributeName' => 'id'],
            ['journeyProgramId', 'default', 'value' => null],

            ['date', 'date', 'format' => 'yyyy-MM-dd'],
            ['date', 'default', 'value' => null],

            ['plan', 'default'],

            ['additional', 'boolean'],
            ['additional', 'default', 'value' => false],
        ];
    }

    public function attributeLabels() {
        $dateFormatterEx = Yii::app()->getComponent('dateFormatterEx');

        return array(
            'date' => 'Дата',
            'plan' => $this->date ? $dateFormatterEx->formatDate($this->date) : 'План на день',
        );
    }

    /**
     * @param $date
     */
    public function setDate($date)
    {
        $this->date = Yii::app()->getComponent('dateFormatterEx')->formatDateToMysqlFormat($date);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return [
            'order' => "{$alias}.date ASC",
        ];
    }
}
