<?php

class CashApiComponent extends CApplicationComponent
{
    /** @var string */
    public $host;

    public $urlTpl = 'https://{{host}}/api/1.0/{{method}}';

    public $token;

    /** @var EHttpClient */
    protected $client;

    /** @var SerializerInterface */
    protected $serializer;


    public function init()
    {
        $this->client = new EHttpClient();
        $this->serializer = Yii::app()->getComponent('serializer');
    }

    public function createOrder(ClientTransactionOrder $order)
    {
        $this->serializer->serialize($order);
        $uri = $this->prepareUri('payments');


        $data = $this->serializer->serialize($order);
        $this->client
            ->setUri($uri)
            ->setRawData(json_encode($data));

        $this->client->setHeaders([
            'Authorization' => $this->token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);

        $response = $this->client->request('POST')->getBody();

        $data = @json_decode($response, true);


        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Invalid json response from Cash Api');
        }

        if (!empty($data)) {
            return $data;
        } else {
            return null;
        }
    }

    public function getOrderStatus(ClientTransactionOrder $order)
    {
        $uri = $this->prepareUri(sprintf('payments/%s', $order->billingId));

        $this->client->setHeaders([
            'Authorization' => $this->token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);

        $response = $this->client
            ->setUri($uri)
            ->request('GET')
            ->getBody();

        $data = @json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Invalid json response from Cash Api');
        }

        if (!empty($data)) {
            return $data;
        } else {
            return null;
        }

    }

    /**
     * @param string $method
     *
     * @return EUriHttp
     * @throws CException
     */
    protected function prepareUri($method)
    {
        $url = str_replace(
            ['{{host}}', '{{method}}'],
            [$this->host, $method],
            $this->urlTpl
        );

        return EUri::factory($url);
    }
}