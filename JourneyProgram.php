<?php

/**
 * @property JourneyProgramDay[] $days
 * @property Partner[] $partners
 * @property JourneyProgramSupport[] $supports
 * @property Request $request
 */
class JourneyProgram extends ActiveRecord
{
    /** @var int */
    public $id;

    /** @var int */
    public $requestId;

    /** @var string */
    public $title;

    /** @var string */
    public $preparationDate;

    /** @var string */
    public $startedAt;

    /** @var string */
    public $finishedAt;

    /** @var bool */
    public $visible;

    /**
     * @param string $className
     * @return ActiveRecord|JourneyProgram
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'journey_program';
    }

    public function primaryKey() {
        return 'id';
    }

    public function relations()
    {
        $alias = $this->getTableAlias();

        return [
            'request' => [self::HAS_ONE, Request::class, ['id' => 'requestId']],
            'days' => [self::HAS_MANY, JourneyProgramDay::class, 'journeyProgramId'],
            'partners' => [self::MANY_MANY, Partner::class, 'journey_program_support(journeyProgramId, partnerId)', 'index' => 'id', 'alias' => "{$alias}_partners"],
            'supports' => [self::HAS_MANY, JourneyProgramSupport::class, 'journeyProgramId'],
        ];
    }

    public function rules() {
        return [
            ['requestId', 'exist', 'className' => Request::class, 'attributeName' => 'id'],
            ['requestId', 'default', 'value' => null],

            ['preparationDate', 'default', 'value' => null],
            ['preparationDate', 'date', 'format' => 'yyyy-MM-dd'],

            ['startedAt', 'default', 'value' => null],
            ['startedAt', 'date', 'format' => 'yyyy-MM-dd'],

            ['title', 'safe'],
            ['title', 'default', 'value' => ''],

            ['finishedAt', 'default', 'value' => null],
            ['finishedAt', 'date', 'format' => 'yyyy-MM-dd'],

            ['visible', 'boolean'],
            ['visible', 'default', 'value' => false],
        ];
    }

    public function attributeLabels() {
        return array(
            'preparationDate' => 'Дата составления программы',
            'requestId' => 'Заявка',
            'partners' => 'Партнеры',
            'visible' => 'Отображается в личном кабинете'
        );
    }


    /**
     * @param string $preparationDate
     */
    public function setPreparationDate($preparationDate)
    {
        /** @var DateFormatterEx $dateFormatterEx */
        $dateFormatterEx = Yii::app()->getComponent('dateFormatterEx');
        $this->preparationDate = $dateFormatterEx->formatDateToMysqlFormat($preparationDate);
    }

    /**
     * @return bool
     */
    public function haveAdditionalDays()
    {
        foreach ($this->days as $day) {
            if ($day->additional) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return $this
     */
    public function toggleVisibility()
    {
        $this->visible = $this->visible ? false : true;
        $this->save();

        return $this;
    }
}
